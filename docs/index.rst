.. nautical documentation master file, created by
   sphinx-quickstart on Thu Jun 18 21:42:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to nautical's documentation!
====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:


error
======
.. automodule:: nautical.error.__init__
   :members:
   :special-members:

io
======
.. automodule:: nautical.io
   :members:

buoy
----
.. automodule:: nautical.io.buoy
   :members:
   :special-members:

sources
-------
.. automodule:: nautical.io.sources
   :members:
   :special-members:

web
-----
.. automodule:: nautical.io.web
   :members:
   :special-members:

location
========
.. automodule:: nautical.location
   :members:
   :special-members:

point
-----
.. automodule:: nautical.location.point
   :members:
   :special-members:

utility
-------
.. automodule:: nautical.location.util
   :members:

noaa 
======

.. automodule:: nautical.noaa.buoy
   :members:

buoy
-----
.. automodule:: nautical.noaa.buoy.buoy
   :members:
   :special-members:

buoy_data
---------
.. automodule:: nautical.noaa.buoy.buoy_data
   :members:
   :special-members:

source
-------
.. automodule:: nautical.noaa.buoy.source
   :members:
   :special-members:

sea_state
=========
.. automodule:: nautical.sea_state.__init__
   :members:
   :special-members:

time
======

conversion
-------------
.. automodule:: nautical.time.conversion
   :members:
   :special-members:

enumerations
-------------
.. automodule:: nautical.time.enums
   :members:
   :special-members:

nautical time
-------------
.. automodule:: nautical.time.nautical_time
   :members:
   :special-members:

units
=====
.. automodule:: nautical.units.units
   :members:
   :special-members:

conversion
------------
.. automodule:: nautical.units.conversion
   :members:
   :special-members: